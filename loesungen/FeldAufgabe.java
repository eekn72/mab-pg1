public class FeldAufgabe {

// main-Methode zum Testen, in Aufgabe nicht gefordert
	public static void main(String[] args) {
		int[] f = { 1, 4, -3, 3, 8, 12, 111 };

		System.out.println(maxDiff(f));
	}

	static int maxDiff(int[] f) {
		if (f.length == 1) {
			return 0;
		}
		int dm = f[1] - f[0];
		for (int i = 2; i < f.length; i++) {
			int d = f[i] - f[i - 1];
			if (d > dm) {
				dm = d;
			}
		}
		return dm;
	}

}
